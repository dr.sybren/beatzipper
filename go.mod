module gitlab.com/dr.sybren/beatzipper

go 1.13

require (
	github.com/charmbracelet/bubbles v0.7.6
	github.com/charmbracelet/bubbletea v0.12.2
	github.com/flytam/filenamify v1.0.0
	github.com/kardianos/osext v0.0.0-20190222173326-2bc1f35cddc0
	github.com/muesli/termenv v0.7.4
	github.com/sirupsen/logrus v1.7.0
)
