package infodat

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2020 Sybren A. Stüvel.
 *
 * This file is part of BeatZipper.
 *
 * BeatZipper is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * BeatZipper is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * BeatZipper.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"encoding/json"
	"io/ioutil"
	"os"
)

// Parse parses an info.dat file and returns its contents.
func Parse(filename string) (Metadata, error) {
	jsonFile, err := os.Open(filename)
	if err != nil {
		return Metadata{}, err
	}
	defer jsonFile.Close()

	fileContents, err := ioutil.ReadAll(jsonFile)
	if err != nil {
		return Metadata{}, err
	}

	var metadata Metadata
	if err := json.Unmarshal(fileContents, &metadata); err != nil {
		return Metadata{}, err
	}

	return metadata, nil
}
