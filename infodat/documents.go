package infodat

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2020 Sybren A. Stüvel.
 *
 * This file is part of BeatZipper.
 *
 * BeatZipper is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * BeatZipper is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * BeatZipper.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

// Metadata contains metadata of BeatSaber maps
type Metadata struct {
	Version         string `json:"_version"` // 2.0.0
	SongName        string `json:"_songName"`
	SongSubName     string `json:"_songSubName"`
	SongAuthorName  string `json:"_songAuthorName"`
	LevelAuthorName string `json:"_levelAuthorName"`
}
