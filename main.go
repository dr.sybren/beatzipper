package main

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2020 Sybren A. Stüvel.
 *
 * This file is part of BeatZipper.
 *
 * BeatZipper is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * BeatZipper is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * BeatZipper.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"flag"
	"fmt"
	"path/filepath"

	"github.com/kardianos/osext"
	"gitlab.com/dr.sybren/beatzipper/tui"
)

var cliArgs struct {
	options tui.ProgramOptions
	version bool
}

func parseCliArgs() {
	flag.BoolVar(&cliArgs.version, "version", false, "Show version number and quit.")
	flag.Parse()

	cliArgs.options.Files = flag.Args()

	exePath, _ := osext.Executable()
	cliArgs.options.ExtractionLocation = filepath.Dir(exePath)
}

func main() {
	parseCliArgs()
	if cliArgs.version {
		fmt.Printf("%s %s\n", tui.ApplicationName, tui.ApplicationVersion)
		return
	}

	tui.Main(cliArgs.options)
}
