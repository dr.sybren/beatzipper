# BeatZipper

Unzips BeatSaber maps from [Beat Savior][BS]. Use at your own risk.


[BS]: [https://www.beatsavior.io/]

## Usage

1. Put the `beatzipper-v{version}.exe` file into `...\steamapps\common\Beat Saber\Beat Saber_Data\CustomLevels`.
2. Put the ZIP files you downloaded from [Beat Savior][BS] there as well.
3. Select all the ZIP files.
4. Drag and drop them on top of `beatzipper-v{version}.exe`.

The program then proceeds to extract each ZIP file into a directory named after
that ZIP file. If it can find an `info.dat` file, it will use the information in
it to rename the directory to something that includes the song name, song
author, and mapper name.


## Copyright & License

Copyright © 2020 by [Sybren A. Stüvel](https://stuvel.eu/).

BeatZipper is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

BeatZipper is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with BeatZipper.  If not, see https://www.gnu.org/licenses/.

The images used in this documentation are created by and copyright © 2020 by Sybren A. Stüvel. They can be freely distributed but only used or otherwise displayed as part of documentation for or demonstration of BeatZipper, and any usage must credit [Sybren A. Stüvel](https://stuvel.eu/) as the original creator.
