# BeatZipper Changelog

## Version 2.0 - 2021-03-05

- Add a Text User Interface (TUI) that shows the progress and possibly an error
  message. This makes the application much easier to use in Windows, where
  otherwise the window would immediately close regardless of whether there was
  an error or not.
- Extract to the location of the beatzipper executable. This allows for the ZIP
  files to remain in a download folder.

## Version 1.2 - released 2020-12-05

- Changed directory format to `{song name}-{song author}-{mapper}-{hash}`.

## Version 1.1 - released 2020-12-05

-  Handle `info.dat` filenames case-insensitively.

## Version 1.0 - released 2020-12-05

- First working version.
