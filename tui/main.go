package tui

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2021 Sybren A. Stüvel.
 *
 * This file is part of BeatZipper.
 *
 * BeatZipper is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * BeatZipper is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * BeatZipper.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"runtime"

	tea "github.com/charmbracelet/bubbletea"
	"github.com/sirupsen/logrus"
)

// ProgramOptions can be passed from the CLI to influence how the program behaves.
type ProgramOptions struct {
	// The files to extract.
	Files []string

	ExtractionLocation string
}

// Main runs the TUI application.
func Main(options ProgramOptions) {
	program := tea.NewProgram(NewModel(options))

	if runtime.GOOS != "windows" {
		program.EnterAltScreen()
	}

	err := program.Start()

	if runtime.GOOS != "windows" {
		program.ExitAltScreen()
	}

	if err != nil {
		logrus.WithError(err).Fatal("alas, there's been an error")
	}
}
