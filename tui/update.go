package tui

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2021 Sybren A. Stüvel.
 *
 * This file is part of BeatZipper.
 *
 * BeatZipper is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * BeatZipper is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * BeatZipper.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"time"

	tea "github.com/charmbracelet/bubbletea"
)

const (
	progressPadding = 2

	// Artificially slow down the program so you can actually see the progress bar progressing.
	slowdown = 125 * time.Millisecond
)

func (m model) Init() tea.Cmd {
	return func() tea.Msg {
		return startupApp(m.options.Files)
	}
}

func (m model) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	// Some messages always need to be processed, regardless of state.
	switch msg := msg.(type) {
	case tea.KeyMsg:
		switch msg.String() {
		case "ctrl+c":
			return m, tea.Quit
		case "q", "enter", " ":
			if m.done || m.err != nil {
				return m, tea.Quit
			}
		}

	case tea.WindowSizeMsg:
		m.progress.Width = msg.Width - progressPadding*2 - 4
		return m, nil

	case errMsg:
		m.err = msg.err
		m.errMessage = msg.message
		return m, nil
	}

	// Regular update handling.
	switch msg.(type) {
	case appStartedMsg:
		m.state = psExtractingFiles
		m.fileIndex = 0
		return m, func() tea.Msg {
			return extractFile(m.options.Files[m.fileIndex], m.options.ExtractionLocation)
		}

	case fileExtractedMsg:
		<-time.After(slowdown)
		m.fileIndex++

		if m.fileIndex < len(m.options.Files) {
			return m, func() tea.Msg {
				return extractFile(m.options.Files[m.fileIndex], m.options.ExtractionLocation)
			}
		}

		m.state = psDone
		m.lastAction = "All files extracted"
		m.done = true
		return m, nil
	}

	return m, nil
}
