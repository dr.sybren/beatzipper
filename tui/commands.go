package tui

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2021 Sybren A. Stüvel.
 *
 * This file is part of BeatZipper.
 *
 * BeatZipper is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * BeatZipper is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * BeatZipper.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	tea "github.com/charmbracelet/bubbletea"
	"github.com/flytam/filenamify"
	"gitlab.com/dr.sybren/beatzipper/infodat"
	"gitlab.com/dr.sybren/beatzipper/unzip"
)

var (
	// ErrNoFiles is returned when no files were given to extract.
	ErrNoFiles = errors.New("no files given to extract")
)

func startupApp(filesToExtract []string) tea.Msg {
	if len(filesToExtract) == 0 {
		return errMsg{"nothing to do", ErrNoFiles}
	}
	return appStartedMsg{}
}

func extractFile(zippath, extractionLocation string) tea.Msg {
	_, zipname := filepath.Split(zippath)

	dirname := unzip.FilenameWithoutExtension(zipname)
	dirpath := filepath.Join(extractionLocation, dirname)

	unzippedFiles, err := unzip.Unzip(zippath, dirpath)
	if err != nil {
		return errMsg{"unable to unzip the ZIP file", err}
	}

	for _, filename := range unzippedFiles {
		if strings.ToLower(filepath.Base(filename)) != "info.dat" {
			continue
		}

		if err := renameDir(dirpath, filename); err != nil {
			return errMsg{"error renaming directory", err}
		}
		break
	}

	return fileExtractedMsg{zippath}
}

func renameDir(dirpath, infoDatFilename string) error {
	meta, err := infodat.Parse(infoDatFilename)
	if err != nil {
		return err
	}

	directory, currentName := filepath.Split(dirpath)
	newName := fmt.Sprintf("%s-%s-%s-%s", meta.SongName, meta.SongAuthorName, meta.LevelAuthorName, currentName[:5])
	newName, err = filenamify.Filenamify(newName, filenamify.Options{Replacement: ""})

	newPath := directory + newName

	if err := os.RemoveAll(newPath); err != nil {
		return err
	}

	if err := os.Rename(dirpath, newPath); err != nil {
		return err
	}

	return nil
}
