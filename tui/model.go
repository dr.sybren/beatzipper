package tui

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2021 Sybren A. Stüvel.
 *
 * This file is part of BeatZipper.
 *
 * BeatZipper is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * BeatZipper is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * BeatZipper.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"github.com/charmbracelet/bubbles/progress"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/sirupsen/logrus"
)

type programState string

const (
	psStarting        programState = "STARTING"
	psExtractingFiles programState = "EXTRACTING_FILES"
	psDone            programState = "DONE"
)

type model struct {
	state   programState
	options ProgramOptions

	lastAction string
	progress   *progress.Model

	fileIndex int
	done      bool

	err        error
	errMessage string
}

// NewModel returns a new TUI model.
func NewModel(options ProgramOptions) tea.Model {
	progress, err := progress.NewModel(progress.WithScaledGradient("#e52b2b", "#2b37e5"))
	if err != nil {
		logrus.WithError(err).Fatal("error not initializing progress model")
	}

	return model{
		state:    psStarting,
		options:  options,
		progress: progress,
	}
}
