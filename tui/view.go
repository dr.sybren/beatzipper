package tui

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2021 Sybren A. Stüvel.
 *
 * This file is part of BeatZipper.
 *
 * BeatZipper is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * BeatZipper is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * BeatZipper.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"fmt"
	"strings"

	"github.com/muesli/termenv"
)

var (
	colorProfile     = termenv.ColorProfile()
	styleBasic       = termenv.Style{}
	styleHeader      = styleBasic.Foreground(colorProfile.Color("#e5882b")).Bold()
	styleRunning     = styleBasic.Foreground(colorProfile.Color("#e5e52b"))
	styleError       = styleBasic.Foreground(colorProfile.Color("#e52b2b")).Bold()
	styleDone        = styleBasic.Foreground(colorProfile.Color("#2be52b")).Bold()
	styleEnter       = styleBasic.Bold()
	styleDimmed      = styleBasic.Foreground(colorProfile.Color("#444444"))
	styleAskingInput = styleBasic.Foreground(colorProfile.Color("#e5e52b")).Bold()
)

func (m model) View() string {
	header := fmt.Sprintf("%s %s", ApplicationName, ApplicationVersion)
	s := styleHeader.Styled(header)
	s += ": "
	switch {
	case m.err != nil:
		s += styleError.Styled("ERROR")
	case m.done:
		s += styleDone.Styled("done")
	default:
		s += styleRunning.Styled("running")
	}
	s += "\n\n"

	s += fmt.Sprintf("        State : %s\n", m.state)
	s += fmt.Sprintf("  Last action : %s\n", m.lastAction)
	s += fmt.Sprintf("Extracting to : %s\n", m.options.ExtractionLocation)

	switch m.state {
	case psExtractingFiles, psDone:
		s += "\n"
		s += fmt.Sprintf(" Files to extract: %d\n", len(m.options.Files))
		s += fmt.Sprintf("        extracted: %d\n", m.fileIndex)
	}

	if m.err != nil {
		styledError := styleError.Styled(fmt.Sprintf("%s; %s", m.errMessage, m.err.Error()))
		s += fmt.Sprintf("\n %s", styledError)
	} else {
		s += m.progressbar()
	}

	var enterMsg string
	if m.err != nil || m.done {
		enterMsg = styleEnter.Styled("Press ENTER to close.")
	} else {
		enterMsg = styleDimmed.Styled("Press CTRL+C to close.")
	}
	s += "\n\n\n" + enterMsg + "\n"

	return s
}

func (m model) progressPercent() (percent float64) {
	if m.done {
		return 1.0
	}

	switch m.state {
	case psStarting:
		return 0.0
	case psDone:
		return 1.0
	case psExtractingFiles:
		percent = float64(m.fileIndex) / float64(len(m.options.Files))
	}

	if percent > 1.0 {
		percent = 1.0
	}
	return
}

func (m model) progressbar() string {
	pad := strings.Repeat(" ", progressPadding)
	return pad + m.progress.View(m.progressPercent())
}
